import React from 'react'
import { Container,Table,Button} from 'react-bootstrap'
import IconDelete from '../icon/IconDelete'
import UpDateIcon from '../icon/UpDateIcon'

export default function TableForm() {
    return (
        <Container>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Chantha</td>
                        <td>chanthapork@gmail.com</td>
                        <td><span><img src="img/222.jpeg" height="100px"/></span></td>
                        <td><div>
                            <Button variant="warning"><UpDateIcon/>Edit</Button>{' '}
                            <Button variant="danger"><IconDelete/>Delete</Button> 
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Nita</td>
                        <td>nita005@gamil.com</td>
                        <td><span><img src="img/333.jpeg" height="100px"/></span></td>
                        <td><div>
                            <Button variant="warning"><UpDateIcon/>Edit</Button>{' '}
                            <Button variant="danger"><IconDelete/>Delete</Button> 
                        </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    )
}
