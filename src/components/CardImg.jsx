import React from 'react'
import { Container,Card} from 'react-bootstrap'
import DisplayImage from './DisplayImage'

export default function CardImg() {
    return (
        <Container>
            <Card>
                <span><Card.Img src="img/111.jpg" height="200px"/></span>
                <Card.Body>
                    <p className="mb-4">Choose image</p>
                    <DisplayImage/>
                </Card.Body>
            </Card>
        </Container>
    )
}
