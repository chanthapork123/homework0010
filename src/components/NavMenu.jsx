import React from 'react'
import { Container,Navbar,Nav,Form,FormControl,Button} from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

export default function NavMenu() {
    return (
        <Container fluid>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#">AMS Managerment</Navbar.Brand>
                <Navbar.Collapse id="navbarScroll">
                    <Nav className="mr-auto my-2 my-lg-0" style={{ maxHeight: '100px' }} navbarScroll >
                    <Nav.Link as={NavLink} to="/home">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/post">Post</Nav.Link>
                    <Nav.Link as={NavLink} to="/user">User</Nav.Link>
                    <Nav.Link as={NavLink} to="/category">Category</Nav.Link>
                    <Nav.Link as={NavLink} to="/author">Author</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                    <FormControl type="search" placeholder="Search" className="mr-2" aria-label="Search" />
                    <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar> 
        </Container>
    )
}
