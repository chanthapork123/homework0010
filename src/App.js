import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavMenu from './components/NavMenu';
import HomeLink from './view/HomeLink';
import { Container } from 'react-bootstrap';
import UserLink from './view/UserLink';
import PostLink from './view/PostLink';
import Category from './view/Category';
import AuthorLink from './view/AuthorLink';

function App() {
  return (
    <Container>
      <Router>
        <NavMenu/>
        <Switch>
          <Route exact path="/home" component={HomeLink}/>
          <Route path="/post" component={PostLink}/>
          <Route path="/user" component={UserLink}/>
          <Route path="/category" component={Category}/>
          <Route path="/author" component={AuthorLink}/>
        </Switch>
      </Router>
  
    </Container>
  );
}

export default App;
