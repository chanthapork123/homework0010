import React from 'react'
import { Container } from 'react-bootstrap'

export default function HomeLink() {
    return (
        <Container className="mt-5">
            <h4>Home</h4>
        </Container>
    )
}
