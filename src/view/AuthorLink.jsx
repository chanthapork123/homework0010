import React, { Component } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import CardImg from '../components/CardImg'
import LoginForm from '../components/LoginForm'
import TableForm from '../components/TableForm'

export default class AuthorLink extends Component {
    render() {
        return (
            <Container className="mt-5">
                <Row>
                    <Col md={8}>
                        <LoginForm/>
                    </Col>
                    <Col md={4}>
                        <CardImg/>
                    </Col>
                </Row>
                <Row className="mt-3">
                    <Col md={12}>
                        <TableForm/>
                    </Col>
                </Row>
            </Container>
        )
    }
}
